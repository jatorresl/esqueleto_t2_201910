package model.data_structures;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class LinkedListIterator<T> implements Iterator<T> {
	Node<T> next;

	public LinkedListIterator(Node<T> firstNode) {
		this.next = firstNode;
	}

	@Override
	public boolean hasNext() {

		return (next != null);
	}

	@Override
	public T next() {

		if (hasNext()) {
			T element = next.getData();
			next = next.getNext();
			return element;
		}

		throw new NoSuchElementException("No hay proximo");
	}
}
