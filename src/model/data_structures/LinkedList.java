package model.data_structures;

import java.util.Iterator;

public class LinkedList<T> implements ILinkedList<T> {

	private Node<T> first;
	private Node<T> last;

	
	private Node<T> current;

	public LinkedList() {
		first = null;
		current = null;
	}

	@Override
	public String toString() {
		return super.toString();
	}

	@Override
	public Iterator<T> iterator() {
		return new LinkedListIterator<>(first);
	}

	@Override
	public Integer getSize() {
		int count = 0;
		Node<T> p = first;
		while (p != null) {
			count++;
			p = p.getNext();
		}

		return count;
	}

	@Override
	public void add(T elemento) {
		Node<T> p = first;
		Node<T> nodeToAdd = new Node<T>(elemento);
		if (p == null) {
			first = nodeToAdd;
			current = first;
			last = first;
			return;
		}		
		
		Node<T> pNext = current.getNext();
		nodeToAdd.setNext(pNext);
		current.setNext(nodeToAdd);
		if (nodeToAdd.getNext() == null) {
			last = nodeToAdd;
		}
	}

	@Override
	public void addAtEnd(T elemento) {
		if (first == null) {
			first = new Node<T>(elemento);
			current = first;
			last = first;
			return;
		}
		Node<T> p = first;
        last.setNext(new Node<T>(elemento));
        last = last.getNext();
//		while (p.getNext() != null) {
//			p = p.getNext();
//		}
//		p.setNext();
	}

	@Override
	public void addAtK(T element, int k) {
		current = first;
		int count = 0;
		Node<T> nodeToAdd = new Node<T>(element);

		if (k == 0) {
			Node<T> pNext = current.getNext();
			first = nodeToAdd;
			first.setNext(pNext);
			return;

		}
		while (current != null) {
			if ((count + 1) == k) {
				Node<T> pNext = current.getNext();
				nodeToAdd.setNext(pNext);
				current.setNext(nodeToAdd);
				return;
			}
			count++;
			current = current.getNext();
		}
		System.out.println("La posicion dada sobrepasa el tamaño del arreglo");

	}

	@Override
	public T getElement() {
		return first.getData();
	}

	@Override
	public T getCurrentElement() {
		return current.getData();
	}

	@Override
	public T delete() {
		current = first;

		if (first == null) {
			return null;
		}
		first = first.getNext();
		current.setNext(null);
		return current.getData();
	}

	@Override
	public T deleteAtK(int k) {
		current = first;
		int count = 0;
		if (k == 0) {
			first = first.getNext();
			return current.getData();
		}
		while (current != null) {
			if (count == k) {
				Node<T> pNext = current.getNext();
				current.setNext(null);
				current.getPrevious().setNext(pNext);
				return current.getData();

			}
			current = current.getNext();
		}
		System.out.println("La posicion dada sobrepasa el tamaño del arreglo");
		return null;
	}

	@Override
	public T next() {

		return current.getNext().getData();
	}

	@Override
	public T previous() {
		return current.getPrevious().getData();
	}

}
