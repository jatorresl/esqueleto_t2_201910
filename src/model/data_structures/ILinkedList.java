package model.data_structures;

/**
 * Abstract Data Type for a linked list of generic objects
 * This ADT should contain the basic operations to manage a list
 * add, addAtEnd, AddAtK, getElement, getCurrentElement, getSize, delete, deleteAtK
 * next, previous
 * @param <T>
 */
public interface ILinkedList<T> extends Iterable<T> {

	Integer getSize();
	
	public void add(T elemento);
	
	public void addAtEnd(T elemento);
	
	public void addAtK(T elemento, int k);
	
	public T getElement();
	
	public T getCurrentElement();
	
	public T delete();
	
	public T deleteAtK(int k);
	
	public T next();
	
	public T previous();


	
	
	
	

}
