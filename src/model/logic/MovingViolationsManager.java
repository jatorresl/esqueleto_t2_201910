package model.logic;



import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import api.IMovingViolationsManager;
import model.vo.VOMovingViolations;
import model.data_structures.LinkedList;

public class MovingViolationsManager implements IMovingViolationsManager {

	private LinkedList<VOMovingViolations> list;
	
	public MovingViolationsManager() {
		list = new LinkedList<>() ;
	}
	public void loadMovingViolations(String movingViolationsFile){
		try (BufferedReader br = new BufferedReader(new FileReader(movingViolationsFile))){
			
			br.readLine();
			String line ;
			while((line = br.readLine())!= null) {
				
				String[] objectValues = line.split(",");
				int id = Integer.parseInt(objectValues[0]);
				int totalPaid = Integer.parseInt(objectValues[9]);
				VOMovingViolations vo = new VOMovingViolations(id,objectValues[14], objectValues[2],
						objectValues[13], totalPaid, objectValues[12], objectValues[15]);
				list.addAtEnd(vo);
				//System.out.println("Agrego "+vo);
				
			}
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		
	}

		
	@Override
	public LinkedList <VOMovingViolations> getMovingViolationsByViolationCode (String violationCode) {
		LinkedList <VOMovingViolations> resp = new LinkedList<>();
		//System.out.println("LISTA ES VACIA ?");
		for (VOMovingViolations voMovingViolations : list) {
			if(voMovingViolations.getViolationCode().equals(violationCode)) {
				resp.addAtEnd(voMovingViolations);
			}
		}

		return resp;
	}

	@Override
	public LinkedList <VOMovingViolations> getMovingViolationsByAccident(String accidentIndicator) {
		LinkedList <VOMovingViolations> resp = new LinkedList<>();
		for (VOMovingViolations voMovingViolations : list) {
			if(voMovingViolations.getAccidentIndicator().equals(accidentIndicator)) {
				resp.add(voMovingViolations);
			}
		}
		return resp;
	}	


}
