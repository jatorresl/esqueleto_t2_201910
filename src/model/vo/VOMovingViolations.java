package model.vo;

/**
 * Representation of a Trip object
 */
public class VOMovingViolations {

	private int objectId;
	
	private String location;
	
	private String ticketIssuedate;
	
	private int totalPaid;
	
	private String accidentIndicator;
	
	private String violationDescription;
	
	private String violationCode;

	public VOMovingViolations(int objectId, String violatioCode,String location, String ticketIssuedate, int totalPaid,
			String accidentIndicator, String violationDescription) {
		super();
		this.objectId = objectId;
		this.location = location;
		this.ticketIssuedate = ticketIssuedate;
		this.totalPaid = totalPaid;
		this.accidentIndicator = accidentIndicator;
		this.violationDescription = violationDescription;
		this.violationCode = violatioCode;
	}


	/**
	 * @return id - Identificador único de la infracción
	 */
	public int objectId() {

		return objectId;
	}	
	
	
	/**
	 * @return location - Dirección en formato de texto.
	 */
	public String getLocation() {

		return location;
	}

	/**
	 * @return date - Fecha cuando se puso la infracción .
	 */
	public String getTicketIssueDate() {

		return ticketIssuedate;
	}
	
	/**
	 * @return totalPaid - Cuanto dinero efectivamente pagó el que recibió la infracción en USD.
	 */
	public int getTotalPaid() {

		return totalPaid;
	}
	
	/**
	 * @return accidentIndicator - Si hubo un accidente o no.
	 */
	public String  getAccidentIndicator() {

		return accidentIndicator;
	}
		
	/**
	 * @return description - Descripción textual de la infracción.
	 */
	public String  getViolationDescription() {

		return violationDescription;
	}
	
	public String  getViolationCode() {

		return violationCode;
	}


	@Override
	public String toString() {
		return "VOMovingViolations [objectId=" + objectId + ", location=" + location + ", ticketIssuedate="
				+ ticketIssuedate + ", totalPaid=" + totalPaid + ", accidentIndicator=" + accidentIndicator
				+ ", violationDescription=" + violationDescription + ", violationCode=" + violationCode + "]";
	}
	
	
}
